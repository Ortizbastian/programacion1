package metodoEstaticoYDeInstancia;

public class MetodoEstaticoYDeInstancia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String instancia1 = new String("Soy un objeto instancia de la clase String");
		String instancia2 = new String("Soy otro objeto instacia de la clase String");
		char x = instancia1.charAt(2);  
		char y = instancia2.charAt(1);
 		char z = instancia2.charAt(0);
 		System.out.println("3er posici�n instancia 1: "+x+"\n2da posici�n instancia 2: "+y+"\n1er posici�n instancia 2: "+z);
 		
 		int a = 23;
 		String valor = String.valueOf(a);
 		System.out.println("El valor de a es: "+valor);
 		
 		String ejemplo = new String ("1234");
 		Integer.parseInt(ejemplo);
 		System.out.println(ejemplo);
 		

	}

}
