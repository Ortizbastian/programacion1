import javax.swing.JOptionPane;

public class CapturaMultiplesExcepciones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {

			String valor = JOptionPane.showInputDialog("Ingresar un entero");
			int divisor = Integer.parseInt(valor);// java.lang.NumberFormatException
			System.out.println(10 / divisor);// java.lang.ArithmeticException
		} catch (NumberFormatException e) {
			// TODO: handle exception
			System.err.println("Se detecto una excepcion: Porfavor introducir un valor numerico");

		} catch (ArithmeticException e) {
			// TODO: handle exception
			System.err.println("Se detecto una excepcion: Division por 0");
		} // fin try catch

	}// fin main

}// finclase
