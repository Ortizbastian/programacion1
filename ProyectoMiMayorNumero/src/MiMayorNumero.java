import javax.swing.JOptionPane;

public class MiMayorNumero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[]num = new int[10];
		int contador;
		int max = 0;
		int total = 3;
		
		//Ventana para que el usuario ingrese 3 numeros 
		for (contador = 0;  contador< total; contador++) {
			
			num[contador]= Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero:puede ingresar hasta "+total+"numeros"));
			
			//Calculamos el numero mayor 
			if((contador == 0)||(num[contador]>max)){
				max = num[contador];
			}
			
		}
		
		//Desplegamos el mayor calculado
		JOptionPane.showMessageDialog(null, "El numero mayor es: "+ max);

	}

}
