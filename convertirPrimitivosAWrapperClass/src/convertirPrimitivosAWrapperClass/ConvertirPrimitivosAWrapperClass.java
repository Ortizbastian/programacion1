package convertirPrimitivosAWrapperClass;

public class ConvertirPrimitivosAWrapperClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer contadorDatos = new Integer(7801);
		int contador = contadorDatos.intValue();
		System.out.println(contador);

		String valorLcd = "65000";
		int valor = Integer.parseInt(valorLcd);
		System.out.println(valor);

		Integer miEntero = new Integer(valor);
		System.out.println(miEntero);
	}

}
