package ejemploConversion;

public class EjemploConversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Float f = new Float(Math.PI);
		String s = f.toString();
		int i = f.intValue();		
		long l = f.longValue();
		float f2 = f.floatValue();
		double d = f.doubleValue();
		
		System.out.println(f);
		System.out.println(f.toString());
		System.out.println(f.intValue());
		System.out.println(f.longValue());
		System.out.println(f.floatValue());
		System.out.println(f.doubleValue());
	}

}
