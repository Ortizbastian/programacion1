package tarea2;

import java.util.Scanner;

public class Tarea2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*restricciones
		 * 1= a�o = 2017
		 * 2= mes 1...12
		 * 3= dia 1 ... 31
		 * 4 = dias del mes 1,3,5,7,8,10,12 = 31
		 *              mes 4,6,9,11 = 30
		 *              mes 2 =28 o 29
		 */
		int dia;
		int mes;
		int ano;
		int totalDias;
		int i;

		System.out.println("Ingrese dia, mes y a�o: ");
		Scanner entrada = new Scanner(System.in);
		dia = Integer.parseInt(entrada.nextLine());
		mes = Integer.parseInt(entrada.nextLine());
		ano = Integer.parseInt(entrada.nextLine());
		if (mes==2) totalDias = 28 - dia;
		else
			if((mes==4) || (mes==6) || (mes==9) || (mes==11))
				totalDias = 30 - dia;
			else totalDias = 31- dia;
		for(i=mes+1 ; i<= 12; i++)
			switch (i){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				totalDias += 31;
				break;
			case 4: 
			case 6:
			case 9:
			case 11:
			    totalDias += 30;
				break;
			case 2:
				totalDias += 28;
			}
		if((ano % 4 == 0) && (mes<=2)){
			totalDias++;
		}
		
		System.out.println("Faltan "+ totalDias+ " dias para fin de a�o");
	}

}
