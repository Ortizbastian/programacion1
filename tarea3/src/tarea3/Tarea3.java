package tarea3;

import javax.swing.JOptionPane;

public class Tarea3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * restricciones 1= a�o = 2017 2= mes 1...12 3= dia 1 ... 31 4 = dias
		 * del mes 1,3,5,7,8,10,12 = 31 mes 4,6,9,11 = 30 mes 2 =28 o 29
		 */
		int dia;
		int mes;
		int anio;
		int totalDias = 0;
		int i;
		do {
			dia = Integer.parseInt(JOptionPane.showInputDialog("Ingresar el dia: "));
		} while (dia < 1 || dia > 31);
		do {
			mes = Integer.parseInt(JOptionPane.showInputDialog("Ingresar el mes: "));
		} while (mes < 1 || mes > 12);
		do {
			anio = Integer.parseInt(JOptionPane.showInputDialog("Ingresar el a�o"));
		} while (anio <1990 || anio >5000);
		if (mes == 2)
			totalDias = 28 - dia;
		else if ((mes == 4) || (mes == 6) || (mes == 9) || (mes == 11))
			totalDias = 30 - dia;
		else
			totalDias = 31 - dia;
		for (i = mes + 1; i <= 12; i++)
			switch (i) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				totalDias += 31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				totalDias += 30;
				break;
			case 2:
				if (((anio % 4 == 0) && !(anio % 100 == 0)) || anio % 400 == 0)
					totalDias = 29;
				else
					totalDias = 28;
				break;

			}JOptionPane.showMessageDialog(null,"Quedan "+totalDias+" dias para fin de a�o", "Respuesta", JOptionPane.PLAIN_MESSAGE);
	}
}
