package ejemploLong;

public class EjemploLong {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Long l = new Long ((long) Math.PI);
		String s = l.toString();
		int i = l.intValue();
		long l2 = l.longValue();
		float f = l.floatValue();
		double d = l.doubleValue();
		
		System.out.println(l);
		System.out.println(s);
		System.out.println(i);
		System.out.println(l2);
		System.out.println(f);
		System.out.println(d);

	}

}
