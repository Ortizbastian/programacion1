package ejemploMath;

public class EjemploMath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int valorAbsoluto;
		double numeroAleatorio;
		double redondeoPositivo;
		double potencia;
		float maximo;
	
		valorAbsoluto = Math.abs(-123);
		numeroAleatorio = Math.random();
		redondeoPositivo = Math.round(123);
		potencia = Math.pow(2, 4);
		maximo =Math.max((float)1e10,(float)309);
		
		System.out.println("El valor absoluto es: "+ valorAbsoluto);
		System.out.println("El numero aleatorio es: "+ numeroAleatorio);
		System.out.println("El redondeo positivo es: "+ redondeoPositivo);
		System.out.println("La potencia es: "+ potencia);
		System.out.println("El valor maximo es: "+ maximo);
		

	}

}
