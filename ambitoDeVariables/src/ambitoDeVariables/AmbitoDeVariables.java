package ambitoDeVariables;

public class AmbitoDeVariables {

	public static void main(String[] args) {
		
		int var1 = 10; //declaramos la variable 1  y la inicializamos con el valor 10.
		int var2 = 0; 
		
		if (var1 <100) { //utilizamos una estructura de control if.
			//int var2 = 20;
			var2 = 20;
		}else {
			//int var2 = 21;
			var2 = 21;
		}
		//imprimimos el valor de la variable 1.
		//acceso a la variable var1 no hay problema
		//para acceder y no arroja error de compilacion.
		System.out.println("Valor de Var1 "+ var1);
		
		
		//imprimimos el valor de la variable 2.
		//caso 1: no es posible acceder a la variable var2 
		//ya que solo existe en el constexto del if, genera 
		//un error de compilacion.
		System.out.println("Valor de Var2 "+ var2);

	}
}
