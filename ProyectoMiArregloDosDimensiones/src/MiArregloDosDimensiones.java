
public class MiArregloDosDimensiones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Declaramos y creamos un arreglo de int con dos dimensiones, de tama�o 10 por 5
		int[][] edades = new int[10][5];
		
		//imprimimos el numero de filas y columnas
		System.out.println("edades.length= "+ edades.length);
		System.out.println("edades[1].length= "+ edades[1].length);
		
		//Desplegamos el valo de cada elemento del arreglo en el arreglo
		for (int i = 0; i < edades.length; i++) {
			System.out.println("\nComenzando Fila" + i);
			for (int j = 0; j < edades[i].length; j++) {
				edades[i][j] = i * j;
				System.out.println(edades[i][j] + "");
				
			}
		}

	}

}
