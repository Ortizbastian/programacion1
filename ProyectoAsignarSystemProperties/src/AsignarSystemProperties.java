import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class AsignarSystemProperties {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		//Asignar un nuevo objeto Properties.
		//Desde el archivo "misProperties.properties"
		
		FileInputStream propFile =new FileInputStream("misProperties.properties");
		Properties p = new Properties(System.getProperties());
		p.load(propFile);
		
		//asignamos una propiedad a traves de metodo setProperty()	
		p.setProperty("miProps","Mi valor guardado en el objeto Properties");
		
		//asignamos el system properties
		System.setProperties(p);
		
		//imprimimos
		System.getProperties().list(System.out);

	}

}
