package ejemploDouble;

public class EjemploDouble {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Double d = new Double(Math.PI);
	    String s = d.toString();
		int i = d.intValue();		
		long l = d.longValue();
		float f = d.floatValue();
		double d2 = d.doubleValue();
		double d3 = Double.parseDouble("3,14592");
		
		System.out.println(d);
		System.out.println(s);
		System.out.println(i);
		System.out.println(l);
		System.out.println(f);
		System.out.println(d2);
		System.out.println(d3);
		

	}

}
