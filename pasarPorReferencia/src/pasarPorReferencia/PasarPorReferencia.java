package pasarPorReferencia;

public class PasarPorReferencia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[]edad={12,13,16};
		for (int i = 0; i < edad.length; i++) {
			System.out.println(edad[i]);
		}
		System.out.println("Invocamos el metodo test:");
		test(edad);
		
		System.out.println("Despues de invocar el metodo test: ");
		
		for (int i = 0; i < edad.length; i++) {
			System.out.println(edad[i]);
		}

	}

	private static void test(int[] edad) {
		System.out.println("Inicio test");
		for (int i = 0; i < edad.length; i++){
			edad[i]=i+50;
		}
		System.out.println("Fin test");
	}

}
