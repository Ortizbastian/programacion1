package fraccionIrreducible;

import java.util.Scanner;

public class FraccionIrreducible {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Ingresar un numero: ");
		
		Scanner sc = new Scanner(System.in);
		Float num = sc.nextFloat();
		
		int decimales = (num.toString().length()-2);
		String mult = "1";
		for (int i = 0; i<decimales; i++) {
			mult+="0";
		}
		
		int denominador = Integer.parseInt(mult);
		int numerador = (int) (num*denominador);
		
		for(int i = 2; i<= denominador+1; i++) {
			if( numerador%i== 0 && denominador%i==0)
			{
				numerador/=i;
				denominador/=i;
				i=2;
			}
		}
		System.out.println(numerador+"/"+denominador);

	}

}
