
public class MiArreglo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Declaramos e inicializamos un arreglo del tipo int con tama�o para 10 elementos
		int[] edades = new int[10];
		
		//Agregamos elementos al arrglo, el indice multiplicado por 10.
		for (int i = 0; i < edades.length; i++) {
			edades[i] = i*10;
		}
		
		//Desplegamos el valor de cada eleento del arreglo
		for (int i = 0; i < edades.length; i++) {
			System.out.println(edades[i]);
		}

	}

}
