package castingPrimitivos;

public class CastingPrimitivos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Ejemplo 1: Casting implicito
		int numInt = 10;
		double numDouble = numInt;
		System.out.println("Int "+numInt+" es explicitanmente casteado(convertido o forzado hacia double) "+numDouble);
		
		//Ejemplo 2: Casting implicito
		int operador1 = 3; 
		int operador2 = 2;
		double division = operador1/operador2;
		System.out.println("El resultado de la division de los dos operadores es: "+ operador1/operador2 +" es casteado o formateado a double: "+division);
	
		//Ejemplo 3: Casting explicito
		double valDouble = 10.12;
		int valInt = (int) valDouble;
		System.out.println("Double "+valDouble+" es explicitamente casteada hacia int "+ valInt);
		
		//Ejemplo 4: 
		double ope1 = 10.2;
		int ope2 = 2;
		int resultado = (int) ope1/ope2;
		System.out.println("El resultado de la division es "+ope1/ope2+" es explicitamente casteado "+resultado);
	}

}
