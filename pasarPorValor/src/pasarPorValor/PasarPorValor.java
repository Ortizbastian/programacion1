package pasarPorValor;

public class PasarPorValor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i=10;
		System.out.println("Inicializamos el metodo main e inicializamos el valor de i = "+ i);
		test(i);		// TODO Auto-generated method
		System.out.println("Valor de i luego del metodo test: "+i);
	}

	private static void test(int i) {
		// TODO Auto-generated method stub
		System.out.println("Valor del parametro i = "+i);
		i=33;
		System.out.println("Finalizamos el metodo test con el valor de i = "+i);
	}

}
