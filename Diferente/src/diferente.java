import javax.swing.JOptionPane;

public class diferente {

	public static void main(String[] args) {
		
		int variable1 = 2;
		int variable2 = 2;
		/*variable1 = 2;
		variable2 = 2;*/
		
		//nombre++ posincremento
		//++nombre preincremento
		/*
		System.out.println(variable1++);
		System.out.println(++variable2);
		System.out.print("Estado final , (variable1): ");
		System.out.println(variable1);
		System.out.print("Estado final , (variable2): ");
		System.out.println(variable2);
	*/
		//TODO utilizar swing
		
		JOptionPane.showMessageDialog(null,
				"Estado variable 1: "+ variable1++ +
				"\nValor total : "+ variable1 +
				"\nEstado variable2 : "+ ++variable2 +
				"\nValor total : "+ variable2, "Respuesta",JOptionPane.PLAIN_MESSAGE);

	}

}

