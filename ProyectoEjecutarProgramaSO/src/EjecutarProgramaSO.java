import javax.management.RuntimeErrorException;

public class EjecutarProgramaSO {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Runtime rt = Runtime.getRuntime();
		Process proc;
		
		try{
			
			if (System.getProperty("os.name").startsWith("Windows")) {
				//Ejecutar un Programa del sistema operativo
				proc =rt.exec("notepad");
				
			}
			
			else{
				proc = rt.exec("gedit");
			}
			
			proc.waitFor();
		}catch(Exception e){
			System.out.println("notepad es un comando desconocido.");
		}
		

	}

}
