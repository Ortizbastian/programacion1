class MiException extends Exception {
}

public class BloqueFinal {

	static int contador = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while (true) {
			try {
				if (contador++ == 0) {
					throw new MiException();
				}

				System.out.println("No hay excepcion");
			} catch (MiException e) {
				System.err.println("Mi excepcion");
			} finally {
				System.out.println("Dentro del bloque Finally");
				if (contador == 2) {
					break;
				}
			}
		}
	}
}
