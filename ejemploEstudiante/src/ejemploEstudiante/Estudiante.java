package ejemploEstudiante;

public class Estudiante {
	//Declaramos las variables de instancias
	
	private String nombre;
	private String apellido;
	private double notaMatematica;
	private double notaLenguaje;
	private double notaCiencias;
	
	//Declaramos la variable Statica o de la Clase
	private static int contadorAlumnos = 0;
	
	//Calcula el promedio de las asignaturas matematica,lenguaje y ciencia
	public double getPromedio() {
		double resultado;
		resultado = (getNotaMatematica() + getNotaLenguaje() + getNotaCiencias())/3;
		return resultado;
	}
	//Retorna el numero de instancias barra objeto de la clase estudiante
	
	public static int getContadorAlumnos() {
		return contadorAlumnos;
		
	}
	
	public static void incrementaContadorAlumno() {
		contadorAlumnos ++;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public double getNotaMatematica() {
		return notaMatematica;
	}

	public void setNotaMatematica(double notaMatematica) {
		this.notaMatematica = notaMatematica;
	}

	public double getNotaLenguaje() {
		return notaLenguaje;
	}

	public void setNotaLenguaje(double notaLenguaje) {
		this.notaLenguaje = notaLenguaje;
	}

	public double getNotaCiencias() {
		return notaCiencias;
	}

	public void setNotaCiencias(double notaCiencias) {
		this.notaCiencias = notaCiencias;
	}
	
	

}
