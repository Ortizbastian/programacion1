package compararString;

public class CompararString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str1, str2;
		
		str1 = "una cadena de texto... que seran comparadas";
		
		str2 = str1;
		System.out.println("El valor de string 1 es: " + str1 + "\nEl valor de string 2 es: " + str2);
		System.out.println("�Son el mismo objeto? " + (str1 == str2));

		str2 = new String(str1);
		System.out.println("Valor string 1: " + str1 + "\nValor string 2: " + str2);
		System.out.println("�Son el mismo objeto? " + (str1 == str2));

		System.out.println("�Tiene el mismo valor? " + str1.equals(str2));

	}

}
