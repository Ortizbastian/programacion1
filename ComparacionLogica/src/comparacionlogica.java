 import javax.swing.JOptionPane;
 
 public class comparacionlogica {
 
 	public static void main(String[] args) {
 		// TODO Auto-generated method stub
 

 		// definimos las variables:
 
 		int variableEntera;
 		byte variableByte;
 		float variableFloat;
 		double variableDouble;
 
 		// asignamos los valores a las variables:
 		variableEntera = -3;
 		variableByte = 5;
 		variableFloat = 1e-10f;
 		variableDouble = 3.14;
 
 		/* se declaran 6 variables logicas, que van a contener la comparacion logica 
 		entre los diferentes valores:
 		*/
 		
 		boolean comparacion1 = variableEntera > variableEntera;
 		boolean comparacion2 = variableEntera < variableByte;
 		boolean comparacion3 = variableByte >= variableFloat;
 		boolean comparacion4 = variableFloat > +variableDouble;
 		boolean comparacion5 = variableDouble != 0;
 		boolean comparacion6 = 1 == variableFloat;
 
 		System.out.println("La comparacion 1 : "+variableEntera+" > "+variableEntera+" "+ comparacion1);
 		System.out.println("La comparacion 2 : "+variableEntera+" < "+variableByte+" "+ comparacion2);
 		System.out.println("La comparacion 3 : "+variableByte+" >= "+variableByte+" "+ comparacion3);
 		System.out.println("La comparacion 4 : "+variableFloat+" > "+ variableDouble+" "+ comparacion4);
 		System.out.println("La comparacion 5 : "+variableDouble+" != "+ 0+" "+ comparacion5);
 		System.out.println("La comparacion 6 : "+1 +" == "+variableFloat+" "+ comparacion6);
 		
 		
 		JOptionPane.showMessageDialog(null, "La comparacion 1 es: "+variableEntera+" > "+variableEntera+" "+comparacion1+
 				"\nLa comparacion 2 es: "+variableEntera+" > "+variableByte+" "+comparacion2+
 				"\nLa comparacion 3 es: "+variableByte+" >= "+variableByte+comparacion3+
 				"\nLa comparacion 4 es: "+variableFloat+" > "+ variableDouble+" "+comparacion4+
 				"\nLa comparacion 5 es :"+variableDouble+ " != "+0+ comparacion5+
 				"\nLa comparacion 6 es :"+1+ " = "+variableFloat+" "+ comparacion6, "Respuesta",JOptionPane.PLAIN_MESSAGE);
 		
 
 	}
 
 }