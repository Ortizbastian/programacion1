
public class TestTiposDatos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Thread objetoHilo = new Thread("Algun proceso Hilo");
		String stringObjeto = "Java Standard Edition";
		char[] charArreglo = { 'a', 'b', 'c' };
		int integerPrimitivo = 4;
		long longprimitivo = Long.MIN_VALUE;
		float floatPrimitivo = Float.MAX_VALUE;
		double doublePrimitivo = Math.PI;
		boolean booleanPrimitivo = true;

		System.out.println(objetoHilo);
		System.out.println(stringObjeto);
		System.out.println(charArreglo);
		System.out.println(integerPrimitivo);
		System.out.println(longprimitivo);
		System.out.println(floatPrimitivo);
		System.out.println(doublePrimitivo);
		System.out.println(booleanPrimitivo);

	}

}
