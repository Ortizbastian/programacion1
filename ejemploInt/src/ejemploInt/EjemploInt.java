package ejemploInt;

public class EjemploInt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Integer i = new Integer((int) Math.PI);
		String s = i.toString();
		int i2 = i.intValue();
		long l = i.longValue();
		float f = i.floatValue();
		double d = i.doubleValue();
		
		System.out.println(i);
		System.out.println(s);
		System.out.println(i2);
		System.out.println(l);
		System.out.println(f);
		System.out.println(d);

	}

}
